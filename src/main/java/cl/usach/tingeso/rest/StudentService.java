package cl.usach.tingeso.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import cl.usach.tingeso.models.Student;
import cl.usach.tingeso.repositories.StudentRepository;

@RestController  
@RequestMapping("/students")
public class StudentService {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Iterable<Student> getAllStudents() {
		try {
			return studentRepository.findAll();
		} catch (NullPointerException ex) {
			return null;
		}
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Student create(@RequestBody Student resource) {
		if(resource.check())
			return studentRepository.save(resource);
		else
			return null;
	}
	 
}

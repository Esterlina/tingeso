package cl.usach.tingeso.models;

import java.util.Calendar;

import javax.persistence.*;

@Entity
@Table(name = "students")
@NamedQuery(name="Student.findAll", query="SELECT s FROM Student s")
public class Student {
    @Id
    @Column(name="id", unique=true, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long studentId;
    
    @Column(name = "rut", nullable = false)
    private String rut;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "career", nullable = false)
    private String career;

    @Column(name = "admission_year", nullable = false)
    private int admissionYear;
    
    public boolean check() {
    	return rut.matches("[1-9]{1,2}.[0-9]{3}.[0-9]{3}-[0-9kK]{1}") &&
    			!name.isEmpty() &&
    			email.matches("[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$") &&
    			!career.isEmpty() &&
    			admissionYear <= Calendar.getInstance().get(Calendar.YEAR);
    }
    
	public long getStudentId() {
		return studentId;
	}
	public void setStudentId(long studentId) {
		this.studentId = studentId;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCareer() {
		return career;
	}
	public void setCareer(String career) {
		this.career = career;
	}
	public int getAdmissionYear() {
		return admissionYear;
	}
	public void setAdmissionYear(int admissionYear) {
		this.admissionYear = admissionYear;
	}
}

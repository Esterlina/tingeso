var app = angular.module('Tingeso',['ngRoute']);

app.config(function($routeProvider){
    $routeProvider
        .when('/student',{
            templateUrl: 'js/views/student.html',
            controller: 'StudentController'
        })
        .when('/students-list',{
            templateUrl: 'js/views/students.html',
            controller: 'StudentsController'
        })
        .otherwise({
            redirectTo: '/index'
        });
});
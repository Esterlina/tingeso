app.controller('StudentController', ['$scope','$http', function($scope,$http) {

    $scope.students = [];
    $scope.newStudent = {};

    $scope.resetNewStudent = function(){
        $scope.newStudent = {
            "rut": "",
            "name":"",
            "email":"",
            "career": "",
            "admissionYear": 0
        }
    }

    $scope.send = function(){
        console.log($scope.newStudent);
        var student = {
            "rut":$scope.newStudent.rut,
            "name":$scope.newStudent.name,
            "email":$scope.newStudent.email,
            "career" : $scope.newStudent.career,
            "admissionYear" : $scope.newStudent.admissionYear
        };
        $http.post('/students',student).then(function(response){
            console.log(response);
            var studentUrl = response.data._links.student.href;
        });
    }


}]);
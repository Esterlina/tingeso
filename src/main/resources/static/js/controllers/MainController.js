app.controller('MainController', ['$scope','$location', function($scope,$location) {

	$scope.title = "Inicio";

	$scope.loadNav = function(){
	    $scope.studentPage = ($location.path() === '/student')?'active':'';
	    $scope.studentsPage = ($location.path() === '/students-list')?'active':'';

		$scope.navigation = [
		{"text":"Administrar alumno",   "link":"#!/student", "class":$scope.studentPage},
		{"text":"Alumnos",   "link":"#!/students-list", "class":$scope.studentsPage},
		];		
	}

	$scope.loadNav();

}]);
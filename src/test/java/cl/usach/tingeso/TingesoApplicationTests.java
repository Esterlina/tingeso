package cl.usach.tingeso;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cl.usach.tingeso.models.Student;
import cl.usach.tingeso.rest.StudentService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TingesoApplicationTests {	
	@Test
	public void studentCheckShouldReturnTrue() {
		Student student = new Student();
		student.setRut("19.522.760-8");
		student.setName("Mario Álvarez");
		student.setEmail("mario.alvarez.m@usach.cl");
		student.setCareer("Informática");
		student.setAdmissionYear(2015);

		Assert.assertTrue("Student check OK!", student.check());
	}

	@Test
	public void storeIncorrentStudentDataShouldReturnNull() {
		Student student = new Student();
		student.setRut("19522760-8");
		student.setName("Mario Álvarez");
		student.setEmail("mario.alvarez.m@usachcl");
		student.setCareer("Informática");
		student.setAdmissionYear(2015);

		StudentService service = new StudentService();
		Assert.assertNull("Student wasn't stored. Incorrect data!", service.create(student));
	}

	@Test
	public void gettersShouldReturnSameSettedData() {
		Student student = new Student();
		String rut = "19.522.760-8";
		String name = "Mario Álvarez";
		String email = "mario.alvarez.m@usach.cl";
		String career = "Informática";
		int admissionYear = 2015;

		student.setRut(rut);
		student.setName(name);
		student.setEmail(email);
		student.setCareer(career);
		student.setAdmissionYear(admissionYear);

		Assert.assertEquals("Same RUT!", rut, student.getRut());
		Assert.assertEquals("Same name!", name, student.getName());
		Assert.assertEquals("Same email!", email, student.getEmail());
		Assert.assertEquals("Same career!", career, student.getCareer());
		Assert.assertEquals("Same admission year!", admissionYear, student.getAdmissionYear());
	}

	@Test
	public void getStudentsShouldReturnNull() {
		StudentService service = new StudentService();
		Assert.assertNull("Not connected to DB!", service.getAllStudents());
	}
}
